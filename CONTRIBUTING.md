VOKABELTRAINER 
================================================================================

Team


Im Team sind Dustin Bastke und Lisa Breuer.

________________________________________________________________________________

Aufgabenverteilung


Lisa hat die Arrays für die Vokabeln angelegt. Danach haben wir uns doch dafür 
entschieden, dass wir eine Textdatei einbinden, diese hat Dustin dann eingebunden. 
Wir beide haben eine Lösung für die Abfrage der Vokabeln gesucht. Der weitere Plan 
sieht vor, dass wir eine Schleife für die Wiederholung
der falschen Vokabeln programmieren. 

Die Aufgabenverteilung ist ausgewogen. Jeder codiert in gleichen Teilen.