VOKABELTRAINER 
================================================================================

ENTWICKLUNGSFORTSCHRITT	
in Schritten


Schritt 1:


#include "string.h"
#include "stdio.h"
#include "stdlib.h"

int main(){
	char deutsch[] = {"Brot"};
	char englisch[] = {"bred"};
	char ergebnis[10];

	printf("Uebersetzen Sie die folgenden deutschen Woerter ins Englische und\nbestaetigen Sie Ihre Eingabe mit Enter!\n\n");
	getchar();

	puts(deutsch);
	gets(ergebnis);
	printf("Ihre Eingabe war: %s\n", ergebnis);
	if (memcmp(ergebnis, englisch, sizeof(ergebnis)) == 0)
		printf("Diese Eingabe ist korrekt!");
	
	else
		printf("Die Eingabe ist nicht korrekt!");

	getchar();
	return 0;
}

____________________________________________________________________________________________________________________

Schritt 2:


#include "string.h"
#include "stdio.h"
#include "stdlib.h"

int main()
{
	char deutsch[7][10] = { "Brot","Marmelade","Honig","Katze","Hund","Maus","Haus" };
	char englisch[7][10] = { "bred","marmelade","honey","cat", "dog", "mouse", "house" };
	char ergebnis[] = { "" };

	printf("Uebersetzen Sie die folgenden deutschen Woerter ins Englische und\nbestaetigen Sie Ihre Eingabe mit Enter!\n\n");
	
	for (int i = 0; i < 7; i++) {
		puts(deutsch[i]);
		gets(ergebnis);
		printf("\n");
		printf("Ihre Eingabe war: %s\n", ergebnis);
		if (memcmp(ergebnis, englisch[i], sizeof(ergebnis[i])) == 0)
			printf("Diese Eingabe ist korrekt!\n\n");

		else
			printf("Die Eingabe ist NICHT korrekt!\nDie richtige Antwort waere: \t%s\n",englisch[i]);
		getchar();
	}
	getchar();
	return 0;
}

_________________________________________________________________________________________________________________

Schritt 3:


Wir haben uns dazu entschieden eine Datei mit den Vokabeln einzubinden. 
So haben wir nicht so große Arrays und es gestaltet sich insgesamt übersichtlicher.


//Bibliotheken
#define _CRT_SECURE_NO_WARNINGS
#include "stdio.h"
#include "string.h"
#include "stdlib.h"

//Variablen
char deutsch[20];
char englisch[20];
char ergebnis[20];
FILE *fp;
int summe = 0;
//Hauptprogramm
int main() {
	//Oeffnen der Datei vokabeln.txt
	fp = fopen("C:\\Users\\Dustin\\Desktop\\vokabeln.txt", "r");

	//Einleitung in das Programm
	printf("Uebersetzen Sie die nachfolgenden deutschen Woerter ins Englische.\nBestaetigen Sie mit Enter um fortzufahren.\n\n");
	printf("Sie erhalten einen Punkt fuer die richtige Uebersetzung und einen Minuspunkt\nfuer eine falsche Uebersetzung.\nDer Punktestand wird am Ende des Durchlaufes ausgegeben.\n\n");
	printf("Uebersetzen Sie die folgenden Vokabeln, bestaetigen Sie mit Enter Ihre Eingabe.\n\n");
	getchar();

	//Lesen und Ausgeben der deutschen Vokabel
	fscanf(fp, "%s", &deutsch[0]);
	puts(deutsch);

	//Lesen und Ausgeben der englischen Vokabel
	fscanf(fp, "%s", &englisch[0]);
	puts(englisch);

	//Eingabeaufforderung und Ausgabe der Uebersetzung
	gets(ergebnis);
	puts(ergebnis);

	//Vergleich ob Eingabe korrekt, inkorrekt ist
	if (strcmp(englisch, ergebnis) == 0) {
		printf("Diese Eingabe ist korrekt!\n");
		summe = summe+1;
		printf("%i", summe);
	}
	else {
		printf("Die Eingabe ist nicht korrekt!\n");
		summe = summe - 1;
		printf("%i", summe);
	}

	//Schließen der Datei Vokabeln.txt
	fclose(fp);
	getchar();
	return 0;
}

________________________________________________________________________________________________________________

Schritt 4:


//Bibliotheken
#define _CRT_SECURE_NO_WARNINGS
#include "stdio.h"
#include "string.h"
#include "stdlib.h"
//Variablen
char deutsch[20];
char englisch[20];
char ergebnis[20];
FILE *fp;
int zaehler = 0;
//Funktionen
int dateioeffnen() {
	fp = fopen("C:\\Users\\Dustin\\Desktop\\vokabeln.txt", "r");
}
int einleitung() {
	printf("Uebersetzen Sie die nachfolgenden deutschen Woerter ins Englische.\nBestaetigen Sie mit Enter um fortzufahren.\n\n");
	printf("Sie erhalten einen Punkt fuer die richtige Uebersetzung und einen Minuspunkt\nfuer eine falsche Uebersetzung.\nDer 
	Punktestand wird am Ende des Durchlaufes ausgegeben.\n\n");
	printf("Uebersetzen Sie die folgenden Vokabeln, bestaetigen Sie mit Enter Ihre Eingabe.\n\n");
}
int abfrage() {
	fscanf(fp, "%s", &deutsch[0]);
	puts(deutsch);
	fscanf(fp, "%s", &englisch[0]);
	puts(englisch);
	gets(ergebnis);
	puts(ergebnis);
}
int vergleich() {
	//Vergleich ob Eingabe korrekt, inkorrekt ist
	if (strcmp(englisch, ergebnis) == 0) {
		printf("Diese Eingabe ist korrekt!\n");
	}
	else {
		printf("Die Eingabe ist nicht korrekt!\n");
	}
}
int rundenzaehler() {
	zaehler = zaehler + 1;
	printf("%i", zaehler);
}
int dateischliessen() {
	fclose(fp);
}



//Hauptprogramm
int main() {
	dateioeffnen();
	einleitung();
	abfrage();
	vergleich();
	rundenzaehler();
	dateischliessen();
	getchar();
	return 0;
}

___________________________________________________________________________________________________________________________

Schritt 4 

// ohne Dateieinbindung/mit Arrays

#include "string.h"
#include "stdio.h"
#include "stdlib.h"

int main()
{
	char *deutsch[] = { "Brot","Marmelade","Honig","Katze","Hund","Maus","Haus", NULL };
	char *englisch[] = { "bred","marmelade","honey","cat", "dog", "mouse", "house", NULL };
	char ergebnis[] = { "" };
	int punkte = 0;

	printf("Uebersetzen Sie die folgenden deutschen Woerter ins Englische und\nbestaetigen Sie Ihre Eingabe mit Enter!\n\n");
	
		for (int i = 0; deutsch[i] != NULL; i++) {
			puts(deutsch[i]);
			gets(ergebnis);
			printf("\n");
			//printf("Ihre Eingabe war: %s\n", ergebnis);
			if (strcmp(ergebnis, englisch[i]) == 0) {
				puts("Diese Eingabe ist korrekt!\n");
				punkte++;
				printf("%d Punkte\n\n\n", punkte);
			}
			else
				printf("Die Eingabe ist NICHT korrekt!\nDie richtige Antwort waere: \t%s\n\n\n", englisch[i]);
		}
	getchar();
	return 0;
}
_________________________________________________________________________________________________________________

Schritt 5


//Bibliotheken
#define _CRT_SECURE_NO_WARNINGS
#include "stdio.h"
#include "string.h"
#include "stdlib.h"
//Variablen
char deutsch[20];
char englisch[20];
char ergebnis[20];
FILE *fp, *fs;
int vokabeln = 5;
int versuche = 0;
int richtig = 0;
int falsch = 0;
//Funktionen
void dateioeffnen() {
	fp = fopen("C:\\Users\\Lisa\\Desktop\\Vokabeln.txt", "r+");
}
void AusgabeDateioeffnen() {
	fs = fopen("C:\\Users\\Lisa\\Desktop\\falscheVokabeln.txt", "r+");
}
void einleitung() {
	printf("Willkommen im Vokabeltrainer Deutsch - Englisch.\n\nDieser Trainer zielt darauf ab,\ndie deutschen Vokabeln in das Englische zu uebersetzen.\n\n");
	printf("Sie erhalten 1 Punkt fuer jede richtige Uebersetzung.\nSie benoetigen 10/10 richtig Uebersetzungen,\num diese Lerneinheit abzuschliessen.\n\n");
	printf("Die falsch uebersetzten Vokabeln werden nach einem Durchlauf erneut abgefragt.\n\n");
	printf("Der Punktestand wird Ihnen am Ende der Einheit ausgegeben.\n\n");
	printf("Uebersetzen Sie die folgenden Vokabeln, bestaetigen Sie mit Enter Ihre Eingabe.\n\n");
	printf("Bestaetigen Sie mit Enter um fortzufahren.\n\n");
	getchar();
}
void abfrage() {
	fscanf(fp, "%s", &deutsch[0]);
	puts(deutsch);
	fscanf(fp, "%s", &englisch[0]);
	gets(ergebnis);
}
void abfragefalsch() {
	fscanf(fs, "%s", &deutsch[0]);
	puts(deutsch);
	fscanf(fs, "%s", &englisch[0]);
	gets(ergebnis);
}
void vergleich() {
	if (strcmp(englisch, ergebnis) == 0) {
		printf("Diese Eingabe ist korrekt!\n\n");
		richtig++;
	}
	else {
		printf("Die Eingabe ist nicht korrekt!\n\n");
		fprintf(fs, "%s\n", deutsch);
		fprintf(fs, "%s\n", englisch);
		falsch++;
	}
	versuche++;
}

void vergleichfalsch() {
	if (strcmp(englisch, ergebnis) == 0) {
		printf("Diese Eingabe ist korrekt!\n\n");
		falsch--;
		richtig++;
	}
	else {
		printf("Die Eingabe ist nicht korrekt!\n\n");
		fprintf(fs, "%s\n", deutsch);
		fprintf(fs, "%s\n", englisch);
	}
}
void dateischliessen() {
	fclose(fp);
	fclose(fs);
}
//Hauptprogramm
int main() {
	dateioeffnen();
	AusgabeDateioeffnen();
	einleitung();
	while (1) {
		abfrage();
		vergleich();
		if (versuche == vokabeln) {
			break;
		}
	}
	printf("Der erste Durchlauf wurde beendet!\nSie haben: %i von %i Vokabeln richtig beantwortet!\n\n", richtig, vokabeln);
	getchar();
	dateischliessen();
	if (falsch >0) {
		AusgabeDateioeffnen();
		while (1) {
			abfragefalsch();
			vergleichfalsch();
			if (falsch == 0) {
				break;
			}
		}
	}
	printf("Sie haben: %i von %i Vokabeln richtig beantwortet!\n\n\n", richtig, vokabeln);
	dateischliessen();
	getchar();
	return 0;
}

___________________________________________________________________________________________________________________________________

ENDSTAND:


//Bibliotheken
#define _CRT_SECURE_NO_WARNINGS
#include "stdio.h"
#include "string.h"
#include "stdlib.h"
//Variablen
char deutsch[20];
char englisch[20];
char ergebnis[20];
FILE *fp, *fs;                  //fp = Zeiger für Txt Datei Vokabeln, fs = Zeiger für Txt Datei falsche Vokabeln
int vokabeln = 20;
int versuche = 0;
int richtig = 0;
int falsch = 0;
//Funktionen
void dateioeffnen() {
	fp = fopen("C:\\Users\\Lisa\\Desktop\\Vokabeln.txt", "r+");               //Oeffnet Txt Datei Vokabeln
}
void falschedateioeffnen() {
	fs = fopen("C:\\Users\\Lisa\\Desktop\\falscheVokabeln.txt", "r+");       //Oeffnet Txt Datei Falsche Vokabeln
}
void einleitung() {
	printf("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");
	printf("                              ****   ****        ****   *   *\n");
	printf("                              *   *  *           *      **  *\n");
	printf("   .:VOKABELTRAINER:.         *   *  **     **   **     * * *\n");
	printf("                              *   *  *           *      *  **\n");
	printf("                              ****   *****       *****  *   *\n");
	printf("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n\n");
	printf("Willkommen im Vokabeltrainer Deutsch - Englisch.\n\nDieser Trainer zielt darauf ab,\ndie deutschen Vokabeln in das Englische zu uebersetzen.\n\n");
	printf("Sie erhalten 1 Punkt fuer jede richtige Uebersetzung.\nSie benoetigen %i / %i richtige Uebersetzungen,\num diese Lerneinheit erfolgreich abzuschliessen.\n\n", vokabeln, vokabeln);
	printf("Die falsch uebersetzten Vokabeln werden nach einem Durchlauf erneut abgefragt.\n\n");
	printf("Der Punktestand wird Ihnen am Ende der Einheit ausgegeben.\nBitte schreiben Sie Umlaute aus, wie z.B. ae, oe, ect.\n\n");
	printf("Uebersetzen Sie die folgenden Vokabeln und \nbestaetigen Sie Ihre Eingabe mit Enter.\n\n");
	printf("_______\n\nDruecken Sie jetzt Enter um fortzufahren.\n\n");
	getchar();
}
void abfrage() {
	fscanf(fp, "%s", &deutsch[0]);                      //liest die Deutsche Vokabel
	puts(deutsch);                                      //Gibt die Deutsche Vokabel aus
	fscanf(fp, "%s", &englisch[0]);                     //liest die Englisch Vokabel
	gets(ergebnis);                                     //verlangt eine Eingabe (englische Vokabel)
}
void abfragefalsch() {                                  //Siehe Abfrage
	fscanf(fs, "%s", &deutsch[0]);
	puts(deutsch);
	fscanf(fs, "%s", &englisch[0]);
	gets(ergebnis);
}
void vergleich() {
	if (strcmp(englisch, ergebnis) == 0) {              //Vergleicht Englische Vokabel mit Eingabe
		printf("Eingabe korrekt!\n\n");                   //Gibt das Resultat aus
		richtig++;                                     //Richtig wird erhöht
	}
	else {
		printf("Eingabe nicht korrekt!\nDie richtige Uebersetzung lautet _%s_\n\n", englisch);             //Gibt Resultat aus
		fprintf(fs, "%s\n", deutsch);                   //Schreibt Deutsche Vokabel in 2. Txt Datei
		fprintf(fs, "%s\n", englisch);                  //Schreibt Englische Vokabel in 2. Txt Datei
		falsch++;                                      //Falsch wird erhoeht
	}
	versuche++;
}
void vergleichfalsch() {                                //Siehe Vergleich
	if (strcmp(englisch, ergebnis) == 0) {
		printf("Eingabe korrekt!\n\n");
		richtig++;
	}
	else {
		printf("Eingabe nicht korrekt!\nDie richtige Uebersetzung lautet _%s_\n\n", englisch);
	}
	versuche++;
}
void dateischliessen() {
	fclose(fp);                                           //Schliesst Txt Datei Vokabeln
	fclose(fs);                                           //Schliesst Txt Datei Falsche Vokabeln
}
//Hauptprogramm
int main() {
	dateioeffnen();
	falschedateioeffnen();
	einleitung();
	while (1) {                                             //Schleife immer true
		abfrage();
		vergleich();
		if (versuche == vokabeln) {                         //Wenn Versuche = der Vokabeln die abgefragt werden
			break;                                          //Wenn der Fall, dann Schleife beenden
		}
	}
	printf("\n********************\n\nSie haben: %i Vokabeln falsch beantwortet!\n\n********************\n\n\n", falsch);
	getchar();
	dateischliessen();
	versuche = 0;                                        //Versuche auf 0 setzen
	if (falsch >0) {                                     //Wenn falsche Antworten gegeben wurden, ansonsten Ende
		falschedateioeffnen();
		while (1) {                                      //Schleife immer true
			abfragefalsch();
			vergleichfalsch();
			if (versuche == falsch) {                    //Wenn Falsche Antworten wieder bei 0,
				break;                                   //dann Schleife beenden
			}
		}
	}
	printf("Sie haben %i / %i Vokabeln richtig uebersetzt. \n\n___Punktestand: %i von %i Punkten___\n\n", richtig, vokabeln, richtig, vokabeln);
	printf("Das entspricht dem aktuellen Wissensstand:\n");
	if (richtig < 10) {
		printf("Sie sollten noch lernen, das reicht noch nicht.");
	}
	else if (richtig < 15 || richtig >= 10) {
		printf("Sie muessen noch ein bisschen lernen.");
	}
	else if (richtig <= 19 || richtig >= 15) {
		printf("Schon gut, aber noch nicht perfekt.");
	}
	else if (richtig = vokabeln) {
		printf("Perfekt!");
	}
	dateischliessen();
	getchar();
	return 0;
}




Vokabeln in der Textdatei Vokabeln.txt

Artenvielfalt
biodiversity
Atmosphaere
atmosphere
Duerre
drought
Erdbeben
earthquake
Erdwaerme
geothermal
Erhaltung
preservation
Gift
poison
Lebewesen
creature
Oekologie
ecology
Rauch
fume
Sauerstoff
oxygen
Schmutz
dirt
Staub
dust
Strahlung
radiation
Umwelt
environment
Umweltschuetzer
environmentalist
Unglueck
disaster
Vermeidung
prevention
Verschmutzung
pollution
Zerstoerung
destruction


Die Textdatei falscheVokabeln.txt ist anfangs leer.