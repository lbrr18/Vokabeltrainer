VOKABELTRAINER 
================================================================================

Funktionalität


Das zu entwickelnde Programm ist ein Vokaltrainer, womit man die Möglichkeit hat, 
bestimmte Vokabeln zu lernen und zu wiederholen.

Es sind 20 Vokabeln vorgegeben, welche man richtig beantworten sollte. 
Dazu hat man zwei Durchläufe Zeit, wenn man eine Vokabel nicht weiß oder falsch eingibt, 
wird diese Vokabel im zweiten Durchlauf wiederholt.
Ziel des Trainings ist, sich mit den Vokabeln zu beschäftigen und selbst zu testen
auf welchem Wissensstand man sich befindet.

_________________________________________________________________________________

Programmier Ablaufplan


1) Textdatei anlegen für die Vokabeln
2) Textdatei anlegen für die falsch beantworteten Vokabeln
3) Beide Textdateien öffnen (zum lesen und schreiben)
4) Programm beschreiben/Funktionalität
5) Vokabeln einlesen, deutsche und englische Vokabeln verbinden
6) eingegebene Vokabel mit der richtigem Ergebnis vergleichen
7) falsche Vokabeln in Textdokument speichern
8) Falsche Vokabeln in einem zweiten Versuch abfragen
9) Ergebnis ausgeben, Punktzahl, Wissensstand-Meldung/Empfehlung

_____________________________________________________________________________________

Größte technische Herausforderung


Die größte technische Herausforderung sehen wir darin, ersteinmal eine 
Vergleichsfunktion zu finden, womit wir die eingegebenen Vokabeln mit der richtigen
vergleichen können. Als nächstes ist herauszufinden welche Abbruchbedingung wir 
in einer Schleife für die Abfrage einbauen bzw. mit welcher wir starten. Außerdem
müssen die falsch beantworteten Vokabeln in eine weitere Textdatei geschrieben werden.